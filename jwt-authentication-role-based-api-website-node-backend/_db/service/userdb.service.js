const db = require('../config/db.config');
const UserInfo = db.userinfo;
const UserDetail = db.userdetail;

module.exports = {
    authenticate,
    getAllUsers,
    getUserById,
    getUserByUserName,
    updateSpecificUser
}

function authenticate(username, password) {
    let getUserRecord = []
    const unionProperty = { ...getUserPropertiesObject('username', username), ...getUserPropertiesObject('password', password) }
    // console.log('userdbService', unionProperty)
    return fetchUsers(unionProperty)
        .then(userInfoResponse => {
            if (userInfoResponse.length > 0) {
                // console.log('userdbService1', userInfoResponse.length)
                getUserRecord = formatUserInfoDetails(userInfoResponse)
                // console.log('userdbService2', getUserRecord.length)
                // return getUserRecord
            }
            else
                console.log("username & password doesn't exist")
            return getUserRecord
        })
        .catch(err => {
            console.log('Error:', err.message)
            return getUserRecord;
        })
    console.log('userdbService3', getUserRecord.length)
    // return getUserRecord;
}

function getAllUsers() {
    let getUserRecord = []
    return fetchUsers({})
        .then(userInfoResponse => {
            if (userInfoResponse.length > 0) {
                getUserRecord = formatUserInfoDetails(userInfoResponse)
                console.log("Users1:", getUserRecord[0])
            } else
                console.log("No Record exist")
            return getUserRecord
        })
        .catch(err => {
            console.log('Error:', err.message)
            return getUserRecord
        })
}

function getUserById(id) {
    let getUserRecord = []
    return fetchUsers(getUserPropertiesObject('id', id))
        .then(userinfoArrayResponse => {
            // if (userinfoArrayResponse.length === 0) {
            //     console.log("user doesn't exists")
            //     return userinfoArrayResponse
            // }
            // return formatUserInfoDetails(userinfoArrayResponse)[0]
            if (userinfoArrayResponse.length > 0) {
                getUserRecord = formatUserInfoDetails(userinfoArrayResponse)
                console.log('userdbService2', getUserRecord.length)
            }
            else
                console.log("user doesn't exist")
            return getUserRecord
        })
        .catch(err => {
            console.log('err:', err.message)
            return getUserRecord
        })
    return getUserRecord;
}

function getUserByUserName(username) {
    let getUserRecord = []
    return fetchUsers(getUserPropertiesObject('username', username))
        .then(userinfoResponseArray => {
            if (userinfoResponseArray.length > 0) {
                getUserRecord = formatUserInfoDetails(userinfoResponseArray)
            }
            else
                console.log("username doesn't exist")
            return getUserRecord
        })
        .catch(err => {
            console.log('err:', err.message)
            return getUserRecord
        })
}

function updateSpecificUser(id, userUpdateData) {
    let getUserRecord = []
    const unionProperty = { ...getUserPropertiesObject('id', id), ...getUserPropertiesObject('username', userUpdateData.username) }
    return fetchUsers(unionProperty)
        .then(userinfoResponseArray => {
            if (userinfoResponseArray.length > 0) {
                // getUserRecord = formatUserInfoDetails(userinfoResponseArray)
                console.log('userdbService2', userinfoResponseArray.length)
                console.log(userinfoResponseArray[0].get('userdetail').get())
                const formattedUpdateData = getFormattedUserInfUpdateData(userinfoResponseArray[0], userUpdateData)
                console.log('formattedUpdateData:', formattedUpdateData)
                // userinfoResponseArray[0].update(formattedUpdateData, { returning: true })
                //     .then(updatedRecord => {
                //         console.log('UpdatedRecord:', updatedRecord[0])
                //     })
            }
            else
                console.log("username not found")
            return getUserRecord
        })
        .catch(err => {
            console.log('err:', err.message)
            return getUserRecord
        })


}

function getFormattedUserInfUpdateData(userinfoInstance, userinfoUpdateData) {
    if (!userinfoInstance) {
        userinfoUpdateData.firstname = userinfoUpdateData.firstname || userinfoInstance.get('userdetail').get('firstname')
        userinfoUpdateData.lastname = userinfoUpdateData.lastname || userinfoInstance.get('userdetail').get('lastname')
        uuserinfoUpdateData.role = userinfoUpdateData.role || userinfoInstance.get('userdetail').get('role')
    }
    return userinfoUpdateData
}

function formatUserInfoDetails(userinfoArray = []) {
    let userinfo = []
    if (typeof userinfoArray == 'object') {
        // console.log('assign')
        userinfo = [userinfoArray]
    }
    userinfo = userinfoArray
    // console.log(typeof userinfo)
    // console.log(userinfoArray.get())
    // console.log('useri', userinfo[0].get())
    let formatUserinfoArray = [userinfo.length];
    for (let i = 0; i < userinfo.length; i++) {
        // console.log('i:', i)
        // console.log(userinfo[0].get('id'))
        const { id, username, password } = userinfo[i].get()
        // console.log('obh:', { userid, username, password })
        formatUserinfoArray[i] = { id, username, password }
        // console.log('subset1:', formatUserinfoArray[i])
        const { firstname, lastname, role } = userinfo[i].get('userdetail')
        Object.assign(formatUserinfoArray[i], { firstname, lastname, role })
        // console.log('subset2:', formatUserinfoArray[i])
    }
    return formatUserinfoArray;
}

function getUserPropertiesObject(userPropertyName, userPropertyValue) {
    let property = {
        'id': { id: userPropertyValue },
        'username': { username: userPropertyValue },
        'password': { password: userPropertyValue },
        'firstname': { firstname: userPropertyValue },
        'lastname': { lastname: userPropertyValue },
        'role': { role: userPropertyValue }
    }
    return property[userPropertyName];
}

function fetchSingleUser(parameter) {
    const { ...factor } = parameter
    console.log('fetchSingleUser:', factor)
    return UserInfo.findOne({
        attributes: ['id', 'username', 'password'],
        // attributes: [['id', 'userid'], 'username', 'password'],
        where: parameter,
        include: [{
            model: UserDetail,
            attributes: ['firstname', 'lastname', 'role']
        }]
    }
    )
    // .then(user => {
    //     console.log('user:', user)
    //     return user;
    // })
}

function fetchUsers(parameterObject) {
    return UserInfo.findAll({
        attributes: ['id', 'username', 'password'],
        // attributes: [['id', 'userid'], 'username', 'password'],
        where: parameterObject,
        include: [{
            model: UserDetail,
            attributes: ['firstname', 'lastname', 'role']
        }]
    }
    )
}

//model instance or value to be passed
function getRecordValue(value) {
    value = value.get() ? value.get() : value
    return value
}