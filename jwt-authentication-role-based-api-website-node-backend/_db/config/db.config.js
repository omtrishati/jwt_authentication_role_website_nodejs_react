const env = require('./env.js');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(env.database, env.username, env.password, {
    host: env.host,
    dialect: env.dialect,
    operatorsAliases: false,

    pool: {
        max: env.max,
        min: env.pool.min,
        acquire: env.pool.acquire,
        idle: env.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// db.company = require('../model/company.model.js')(sequelize, Sequelize);
// db.product = require('../model/product.model.js')(sequelize, Sequelize);
db.userinfo = require('../model/userinfo.model')(sequelize, Sequelize);
db.userdetail = require('../model/userdetail.model')(sequelize, Sequelize);
db.roles = require('../model/roles.model')(sequelize, Sequelize);

// Here we can connect companies and products base on company'id
// db.company.hasMany(db.product, { foreignKey: 'fk_companyid', sourceKey: 'uuid' });
// db.product.belongsTo(db.company, { foreignKey: 'fk_companyid', targetKey: 'uuid' });

db.userinfo.hasOne(db.userdetail, { foreignKey: 'fk_userinfoid', sourceKey: 'id', onDelete: 'cascade' });
db.userdetail.belongsTo(db.userinfo, { foreignKey: 'fk_userinfoid', targetKey: 'id' });

module.exports = db;