const db = require('../config/db.config');
const UserInfo = db.userinfo;
const UserDetail = db.userdetail;
let users = []

exports.init = (req, res) => {
    UserInfo.create({
        username: 'user2',
        password: 'user2',
        userdetail: [{
            firstname: 'user2_f',
            lastname: 'user2_l'
        }]
    }, {
        include: [UserDetail]
    }
    )
        .then(data => {
            console.log('data created');
            res.send({ msg: 'data created' });
        })
        .catch(err => {
            console.log(err);
            res.send({ err: err });
        })
}

exports.create = (req, res) => {
    // let userinfo;
    // console.log('name', req.params);
    // const username = req.body.username;
    // console.log('username:', username);
    // console.log('role:', req.body.role);
    // console.log('params:', req.body.params);

    UserInfo.create({
        username: req.body.username,
        password: req.body.password,
        userdetail: [{
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            role: req.body.role
        }]
    }, {
        include: [UserDetail]
    }
    )
        // .then(createUserInfo => {
        //     userinfo = createUserInfo;
        //     console.log('userinfo created');
        //     return UserDetail.create({
        //         firstname: 'user_f',
        //         lastname: 'user_l',
        //         fk_userinfoid: userinfo.id
        //     })
        // })
        .then(userdetail => {
            res.send(userdetail);
        })
        .catch(err => {
            console.log(err);
            res.send({ err: err.name });
        })
}

exports.findAll = (req, res) => {
    UserInfo.findAll({
        attributes: [['id', 'userid'], 'username', 'password'],
        include: [{
            model: UserDetail,
            // where: { fk_userinfoid: db.Sequelize.col('userinfo.id') },
            attributes: ['firstname', 'lastname', 'role']
        }]
    })
        .then(userinfo => {
            // console.log('log', formatUserInfoDetails(userinfo)[0])
            // console.log('userinfo:', userinfo.length)
            // console.log('data:', getRecordValue(userinfo[0].get('userdetail')))
            const getFormattedUserInfoObject = formatUserInfoDetails(userinfo)
            res.send(getFormattedUserInfoObject);
        })
        .catch(err => {
            console.log('userinfo:', err)
            res.send({ err: err.message });
        })
}

function formatUserInfoDetails(userinfoArray = []) {
    let userinfo = []
    if (typeof userinfoArray == 'object') {
        console.log('assign')
        userinfo = [userinfoArray]
    }
    // console.log(typeof userinfo)
    // console.log(userinfoArray.get())
    // console.log('useri', userinfo[0].get())
    let formatUserinfoArray = [userinfo.length];
    for (let i = 0; i < userinfo.length; i++) {
        // console.log('i:', i)
        // console.log(userinfo[0].get('id'))
        const { userid, username, password } = userinfo[i].get()
        // console.log('obh:', { userid, username, password })
        formatUserinfoArray[i] = { userid, username, password }
        // console.log('subset1:', formatUserinfoArray[i])
        const { firstname, lastname, role } = userinfo[i].get('userdetail')
        Object.assign(formatUserinfoArray[i], { firstname, lastname, role })
        // console.log('subset2:', formatUserinfoArray[i])
    }
    return formatUserinfoArray;
}

exports.findByUsername = (req, res) => {
    UserInfo.findAll({
        attributes: [['id', 'userid'], 'username', 'password'],
        where: { username: req.params.username },
        include: [{
            model: UserDetail,
            attributes: ['firstname', 'lastname', 'role']
        }]
    })
        .then(userinfo => {
            // console.log(userinfo.length)
            res.send(userinfo);
        })
        .catch(err => {
            res.send({ err: err.message });
        })
}

exports.findByUserId = (req, res) => {
    const { ...data } = req.params;
    // console.log('data:', data)
    fetchSingleUser({ id: parseInt(data.id) })
        .then(record => {
            if (!record) {
                console.log('user does not exist')
                res.send({ msg: 'user does not exist' })
            }
            // console.log(record)
            const getFormattedUserInfoObject = formatUserInfoDetails(record)
            res.send(getFormattedUserInfoObject[0]);
        })
        .catch(err => {
            console.log(err)
            res.send(err.name)
        })
}

exports.update = (req, res) => {
    const { ...data } = req.body;
    // console.log(data)
    // var user = UserInfo.build({ data })
    // user.save().then(updatedUserInfo => {
    //     console.log(updatedUserInfo.get())
    //     res.send('updated')
    // }).catch(err => {
    //     res.send('failed')
    // })

    fetchSingleUser(data.username)
        .then(record => {
            if (!record) {
                console.log('record does not exist')
                res.send({ msg: 'user does not exist' })
            }
            else {
                console.log('recordr:', record.getDataValue('username'))
                console.log('recordr:', record.length)
                return record.get('userdetail')
            }
        })
        // .then(userdetail => {
        //     // console.log('userdetail:', userdetail.get())
        //     res.send('updated')
        // })
        .catch(err => {
            res.send(err.name)
        })
    // fetchUsers(data.username)
    //     .then(record => {
    //         // console.log('recordr:', record.getDataValue('username'))
    //         console.log('recordr:', record[0])
    //         // return record.get('userdetail')
    //     }).
    //     then(userdetail => {
    //         // console.log('userdetail:', userdetail.get())
    //         res.send('updated')
    //     }).catch(err => {
    //         res.send(err)
    //     })
    UserInfo.update(
        {
            username: req.body.username,
            // password: req.body.password,

            userdetail: [{
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                // role: req.body.role

            }]
        }, {
        include: [UserDetail],
        where: { username: data.username },
        returning: true
    }
    ).then(updated => {
        // console.log(updated[0])
        console.log(updated[1][0])
        res.send('updated:')
    }).catch(err => {
        console.log(err)
        res.send(err)
    })
}


exports.deleteByUsername = (req, res) => {
    const username = req.body.username
    console.log('username:', req.body.username)
    UserInfo.findOne({
        where: { username: req.params.username },
        include: [{
            model: UserDetail,
        }]
    }
    )
        .then(selectedUser => {
            console.log(selectedUser.get().userdetail.get())
            // UserInfo.destroy(selectedUser)
            selectedUser.destroy()
            selectedUser.get().userdetail.destroy()
            res.send('deleted')
        })
}

exports.deleteByUser = (req, res) => {
    const username = req.params.username
    UserInfo.destroy({
        where: { username: username },
        include: [{
            model: UserDetail,
        }]
    }
    )
    // .then(selectedUser => {
    //     // console.log(selectedUser.get().userdetail.get())
    //     console.log(selectedUser)
    //     // UserInfo.destroy(selectedUser)
    //     res.send('deleted')
    // })
}


function fetchSingleUser(parameter) {
    const { ...factor } = parameter
    console.log('fetchSingleUser:', factor)
    return UserInfo.findOne({
        where: parameter,
        include: [{
            model: UserDetail,
        }]
    }
    )
    // .then(user => {
    //     console.log('user:', user)
    //     return user;
    // })
}

function fetchUsers(parameter) {
    return UserInfo.findAll({
        where: { username: parameter },
        include: [{
            model: UserDetail,
        }]
    }
    )
}

//model instance or value to be passed
function getRecordValue(value) {
    value = value.get() ? value.get() : value
    return value
}
