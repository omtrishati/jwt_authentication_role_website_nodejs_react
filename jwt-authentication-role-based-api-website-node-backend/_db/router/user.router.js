module.exports = function (app) {

    const users = require('../controller/user.controller');

    // Init data: Companies & Products
    app.get('/api/user/init', users.init);


    app.post('/api/user/create', users.create);
    app.put('/api/user/update', users.update);
    app.get('/api/user/:username', users.findByUsername);
    app.get('/api/user/id/:id', users.findByUserId);
    // app.delete('/api/user/:username', users.deleteByUsername);
    app.delete('/api/user/:username', users.deleteByUser);

    // Retrieve all Companies
    app.get('/api/user', users.findAll);
}