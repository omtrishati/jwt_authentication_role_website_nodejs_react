module.exports = (sequelize, type) => {
    var usersinfo = sequelize.define('usersinfo', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        username: type.STRING,
        password: type.STRING
    },
        {
            freezeTableName: true
        })
    return usersinfo;
}
