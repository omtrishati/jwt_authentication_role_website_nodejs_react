module.exports = (sequelize, type) => {
    var roles = sequelize.define('role', {
        role: {
            type: type.ENUM,
            values: ['Admin', 'User', 'Member'],
            defaultValue: 'User'
        }
    }, { tableName: 'role', timestamps: false })

    return roles;
}