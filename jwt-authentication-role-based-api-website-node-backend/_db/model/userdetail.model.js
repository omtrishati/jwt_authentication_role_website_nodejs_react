module.exports = (sequelize, type) => {
    var userdetail = sequelize.define('userdetail', {
        // userinfo_id: {
        //     type: type.INTEGER,
        //     primaryKey: true,
        //     autoIncrement: true
        // },
        // user_id: {
        //     type: type.INTEGER,
        // }
        firstname: {
            type: type.STRING
        },
        lastname: {
            type: type.STRING
        },
        role: {
            type: type.STRING
        }
    },
        {
            freezeTableName: true
        });
    return userdetail;
}
