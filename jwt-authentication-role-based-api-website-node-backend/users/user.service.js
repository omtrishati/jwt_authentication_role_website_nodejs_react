const config = require('config.json');
const jwt = require('jsonwebtoken');
const Role = require('_helpers/role');
const debug = require('debug');
const log = debug('user.service:log');

// const userDBService = require('../_db/controller/user.controller.data');
const userDBService = require('../_db/service/userdb.service')


// const users = [
//     { id: 1, username: 'admin', password: 'admin', firstName: 'Admin', lastName: 'User', role: Role.Admin },
//     { id: 2, username: 'user', password: 'user', firstName: 'Normal', lastName: 'User', role: Role.User },
//     { id: 3, username: 'member1', password: 'member1', firstName: 'Normal', lastName: 'member', role: Role.Member }

// ]

module.exports = {
    authenticate,
    getAll,
    getById,
    getByUsername,
    updateSpecificUser
};

async function authenticate({ username, password }) {
    log('authenticate fn => username: %s', username);
    log('authenticate fn => password: %s', password);
    const users = await userDBService.authenticate(username, password)
    console.log(users)
    const user = users.find(u => u.username === username && u.password === password);

    if (user) {
        const token = jwt.sign({ sub: user.id, role: user.role }, config.secret, { expiresIn: 600 });
        const { password, ...userWithoutPassword } = user;
        // log('authenticate fn => password: %s', password);
        // log('authenticate fn => userWithoutPassword: %s', userWithoutPassword);
        return {
            ...userWithoutPassword,
            token
        };
    }
}

async function getAll() {
    log('getAll fn => ');
    const users = await userDBService.getAllUsers()
    return users.map(u => {
        const { password, ...userWithoutPassword } = u;
        // log('getAll fn => password: %s', password);
        // log('getAll fn => userWithoutPassword: %s', userWithoutPassword);
        return userWithoutPassword;
    });
}

async function getById(id) {
    log('getById fn => id: %s', id);
    const users = await userDBService.getUserById(parseInt(id))
    const user = users.find(u => u.id === parseInt(id));
    if (!user) return;
    const { password, ...userWithoutPassword } = user;
    // log('getById fn => password: %s', password);
    // log('getById fn => userWithoutPassword: %s', userWithoutPassword);
    return userWithoutPassword;
}

async function getByUsername(username) {
    log('getByUsername fn => username: %s', username);
    const users = await userDBService.getUserByUserName(username)
    const user = users.find(u => u.username = username)
    if (!user) return;
    const { password, ...userWithoutPassword } = user;
    return userWithoutPassword;
}

async function updateSpecificUser(id, userupdateData) {
    log('getByUsername fn => id: %s', id);
    log('getByUsername fn => username: %s', userupdateData.username);

    const users = await userDBService.updateSpecificUser(id, userupdateData)
    const user = users.find(u => u.username === userupdateData.username)
    if (!user) return;
    const { password, ...userWithoutPassword } = user;
    return userWithoutPassword

}