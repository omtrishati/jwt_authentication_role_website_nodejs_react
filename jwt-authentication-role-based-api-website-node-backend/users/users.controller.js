const express = require('express');
const router = express.Router();
const userService = require('./user.service');
const authorize = require('_helpers/authorize');
const Role = require('_helpers/role');
const debug = require('debug');
const log = debug('users.controller:log');

// routes
router.post('/authenticate', authenticate);     // public route
router.get('/', authorize(Role.Admin), getAll); // admin only
router.get('/:id', authorize(), getById);       // all authenticated users
router.get('/username/:username', authorize(), getByUsername);// all authenticated users
router.put('/', authorize(), updateSpecificUser);// all authenticated users
module.exports = router;


function authenticate(req, res, next) {
    log('authenticate fn=>');
    userService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    log('getAll fn=>');
    userService.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
}

function getById(req, res, next) {
    const currentUser = req.user;
    log('getById fn=> currentUser: %s', currentUser);
    const id = parseInt(req.params.id);

    // only allow admins to access other user records
    if (id !== currentUser.sub && currentUser.role !== Role.Admin) {
        return res.status(401).json({ message: 'Unauthorized' });
    }

    userService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function getByUsername(req, res, next) {
    const currentUser = req.user;
    log('getByUsername fn=> currentUser: %s', currentUser);

    // userService.getByUsername(req.params.username)
    //     .then(user => user ? user.json() : res.sendStatus(404))
    //     .catch(err => { next(err) });

    userService.getByUsername(req.params.username)
        .then(user => {
            if (user) {
                // only allow admins to access others record
                if (user.id !== currentUser.sub && currentUser.role !== Role.Admin)
                    return res.status(401).json({ message: 'Unauthorized' })
                res.json(user)
            }
            else {
                res.sendStatus(404)
            }
        })
        .catch(err => { next(err) });

    // const user = await userService.getByUsername(req.params.username)
    // if (!user) {
    //     res.sendStatus(404)
    // }
    // else {
    //     // only allow user to see their own record
    //     if (user.username !== username && currentUser.role !== user.role) {
    //         return res.status(401).json({ message: 'Unauthorized' });
    //     }
    //     res.json(user)
    // }
}

function updateSpecificUser(req, res, next) {
    const currentLoggedInUser = req.user;
    log('getByUsername fn=> currentLoggedInUser: %s', currentLoggedInUser);

    const id = parseInt(currentLoggedInUser.sub)

    userService.updateSpecificUser(id, req.body)
        .then(user => {
            if (user) {
                res.json(user)
            }
            else
                res.sendStatus(404)
        })
        .catch(err => { next(err) });
}