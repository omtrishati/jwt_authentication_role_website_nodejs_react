require('rootpath')();
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const errorHandler = require('_helpers/error-handler');
const debug = require('debug');
// const logError = debug('server:error');
const log = debug('server:log');


// logError('Server Error');
log('Server log');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// api routes
app.use('/users', require('./users/users.controller'));

// global error handler
app.use(errorHandler);

// start server
const port = process.env.NODE_ENV === 'production' ? 80 : 9000;
const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});


// var express = require('express');
// var app = express();
// var bodyParser = require('body-parser');
// app.use(bodyParser.json())

// const db = require('./_db/config/db.config');

// // force: true will drop the table if it already exists
// db.sequelize.sync({ force: false }).then(() => {
//     console.log('Drop and Resync with { force: true }');
// });

// require('./_db/router/user.router')(app);

// // Create a Server
// var server = app.listen(9000, function () {

//     var host = server.address().address
//     var port = server.address().port

//     console.log("App listening at http://%s:%s", host, port)
// })