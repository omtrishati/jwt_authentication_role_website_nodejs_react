const debug = require('debug');
const logError = debug('error');
module.exports = errorHandler;

function errorHandler(err, req, res, next) {
    if (typeof (err) === 'string') {
        // custom application error
        logError('String error', err);
        return res.status(400).json({ message: err });
    }

    if (err.name === 'UnauthorizedError') {
        // jwt authentication error
        logError('Error', err);
        return res.status(401).json({ message: 'Invalid Token' });
    }

    // default to 500 server error
    logError('jsonerror', err);
    return res.status(500).json({ message: err.message });
}